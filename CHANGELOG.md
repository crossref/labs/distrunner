# Changelog

## 1.4.1: 2023-10-27
* Fix to null AWS connector object bug

## 1.4.0: 2023-10-17
* Add abilitu to specify the disk size on workers.

## 1.2.1: 2023-06-15
* Add ability to retrieve AWS Connector object.

## 1.2.0: 2023-06-15
* Add ability to name software environment.

## 1.1.0: 2023-06-14
* Add ability to specify software environment.

## 1.0.0: 2023-06-14
* Rewrite using coiled.

## 0.0.46: 2023-03-13
* Add on_aws and on_airflow helper shortcuts.

## 0.0.41: 2023-03-10
* Bugfixes.

## 0.0.36: 2023-03-10
* Add ability to specify task permissions in Fargate mode.

## 0.0.35: 2023-03-10
* Add ability to run on Airflow in local mode.

## 0.0.15: 2023-03-09
* AWS bootstrapper. Will install git for you on an AWS MWAA box.

## 0.0.14: 2023-03-09
* Add test suite
* Bugfixes

## 0.0.2: 2023-03-08
* Add verbosity control

## 0.0.1: 2023-03-08
* First release
